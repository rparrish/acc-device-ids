README
=======

This section should include the high-level overview of the project or request.

# Prompt

 - What is being requested? Purpose or use?
 - Population of interest - Inclusions & Exclusions
 - Intervention or comparision groups?
 - Outcomes/Variables?
 - Date Ranges? 

 
# Sources

 - List of databases (EDW, ClinicalAnalytics, Apollo, etc.)
 - List of other data sources (Epic Reporting Workbench, InfoView, etc.)
 - `r getwd()`

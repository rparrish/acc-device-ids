README.md
======================




## NCDR CathPCI data files

Downloaded manually from: 
https://www.ncdr.com/webncdr/CathPCI/Resources/technologydownloads

V4 Intracoronary Devices by Name  
V4 Closure Devices by Name  
V4 Medications by Name  


## NCDR CathPCI data files
https://www.ncdr.com/WebNCDR/icd/resources/technology-downloads

V2.2 Defib Device Master File  
V2.2 Lead Device Master File  


## Lawson data files

lawson.csv
